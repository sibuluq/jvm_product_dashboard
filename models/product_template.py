# This file is part of Product Dashboard (jvm_product_dashboard)
#
# Copyright 2023 Sunu Haeriadi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from odoo import api, fields, models
from odoo.tools import date_utils

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    @api.model
    def get_data(self):
        total_product = self.env['product.template'].search_count([
            ('type', '=', 'product')
        ])
        discontinued_product = self.env['product.template'].search_count([
            '&',
            ('type', '=', 'product'),
            ('purchase_ok', '=', False)
        ])
        expired_product = self.env['product.template'].search_count([
            '&',
            '&',
            ('type', '=', 'product'),
            ('validity_date', '<', fields.Datetime.today()),
            ('purchase_ok', '=', True)
        ])
        expiring_product = self.env['product.template'].search_count([
            '&',
            '&',
            ('type', '=', 'product'),
            ('validity_date', '>=', fields.Datetime.today()),
            ('validity_date', '<=', date_utils.add(fields.Datetime.today(), days=14))
        ])
        updated_product = self.env['product.template'].search_count([
            '&',
            ('type', '=', 'product'),
            ('write_date', '>=', date_utils.subtract(fields.Datetime.today(), days=30))
        ])
        new_product = self.env['product.template'].search_count([
            '&',
            ('type', '=', 'product'),
            ('create_date', '>=', date_utils.subtract(fields.Datetime.today(), days=30))
        ])

        return {
            'total_product': total_product,
            'discontinued_product': discontinued_product,
            'expired_product': expired_product,
            'expiring_product': expiring_product,
            'updated_product': updated_product,
            'new_product': new_product
        }
