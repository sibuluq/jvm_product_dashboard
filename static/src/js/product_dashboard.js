/*
# This file is part of Product Dashboard (jvm_product_dashboard)
#
# Copyright 2023 Sunu Haeriadi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

odoo.define(
    'jvm_product_dashboard.product_dashboard',
    function (require) {
        'use strict';

        var AbstractAction = require('web.AbstractAction');
        var core = require('web.core');
        var rpc = require('web.rpc');
        var ajax = require('web.ajax');

        var _t = core._t;

        /* Implementing a new client action. */
        var ProductDashboard = AbstractAction.extend( {
            template: 'ProductDashboard',

            events: {
                'click #total-product': 'openProductList',
                'click #discontinued-product': 'openDiscontinuedList',
                'click #expired-product': 'openExpiredList',
                'click #expiring-product': 'openExpiringList',
                'click #updated-product': 'openUpdatedList',
                'click #new-product': 'openNewList'
            },

            init: function (parent, context) {
                this._super(parent, context);
            },

            willStart: function () {
                let self = this;

                return $.when(ajax.loadLibs(this), this._super()).then(function () {
                    return self.fetchData();
                });
            },

            fetchData: function () {
                let self = this

                let result = this._rpc({
                    model: 'product.template',
                    method: 'get_data'
                }).then(function (record) {
                    self.total_product = record['total_product'],
                    self.discontinued_product = record['discontinued_product'],
                    self.expired_product = record['expired_product'],
                    self.expiring_product = record['expiring_product'],
                    self.updated_product = record['updated_product'],
                    self.new_product = record['new_product']
                });

                return $.when(result);
            },

            openProductList: function (event) {
                let self = this;

                event.stopPropagation();
                event.preventDefault();

                this.do_action({
                    name: _t("Product"),
                    type: 'ir.actions.act_window',
                    res_model: 'product.template',
                    view_mode: 'tree,form',
                    views: [[false, 'list'], [false, 'form']],
                    domain: [['type', '=', 'product']],
                    target: 'current'
                });
            },

            openDiscontinuedList: function (event) {
                let self = this;

                event.stopPropagation();
                event.preventDefault();

                this.do_action({
                    name: _t("Discontinued Product"),
                    type: 'ir.actions.act_window',
                    res_model: 'product.template',
                    view_mode: 'tree,form',
                    views: [[false, 'list'], [false, 'form']],
                    domain: ['&', ['purchase_ok', '=', false], ['type', '=', 'product']],
                    target: 'current'
                })
            },

            openExpiredList: function (event) {
                let self = this;
                let today = self.getCurrentDate().toISOString().split('T')[0];

                event.stopPropagation();
                event.preventDefault();

                this.do_action({
                    name: _t( "Expired Product Price" ),
                    type: 'ir.actions.act_window',
                    res_model: 'product.template',
                    view_mode: 'tree,form',
                    views: [ [ false, 'list' ], [ false, 'form' ] ],
                    domain: ['&', '&', ['validity_date', '<', today], ['purchase_ok', '=', true], ['type', '=', 'product'] ],
                    target: 'current'
                });
            },

            openExpiringList: function name(event) {
                let self = this;
                let date = self.getCurrentDate();
                let today = date.toISOString().split('T')[0];
                let future = new Date(date.setDate(date.getDate() + 14)).toISOString().split('T')[0];

                event.stopPropagation();
                event.preventDefault();

                this.do_action({
                    name: _t("Expiring Product Price"),
                    type: 'ir.actions.act_window',
                    res_model: 'product.template',
                    view_mode: 'tree,form',
                    views: [[false, 'list'], [false, 'form']],
                    domain: ['&', '&', ['validity_date', '<=', future], ['validity_date', '>=', today], ['type', '=', 'product']],
                    target: 'current'
                });
            },

            openUpdatedList: function (event) {
                let self = this;
                let date = self.getCurrentDate();
                let today = date.toISOString().split('T')[0];
                let past = new Date(date.setDate(date.getDate() - 30)).toISOString().split('T')[0];

                event.stopPropagation();
                event.preventDefault();

                this.do_action({
                    name: _t("Updated Product"),
                    type: 'ir.actions.act_window',
                    res_model: 'product.template',
                    view_mode: 'tree,form',
                    views: [[false, 'list'], [false, 'form']],
                    domain: ['&', ['write_date', '>=', past], ['type', '=', 'product']],
                    target: 'current'
                });
            },

            openNewList: function (event) {
                let self = this;
                let date = self.getCurrentDate();
                let today = date.toISOString().split('T')[0];
                let past = new Date(date.setDate(date.getDate() - 30)).toISOString().split('T')[0];

                event.stopPropagation();
                event.preventDefault();

                this.do_action({
                    name: _t("New Product"),
                    type: 'ir.actions.act_window',
                    res_model: 'product.template',
                    view_mode: 'tree,form',
                    views: [[false, 'list'], [false, 'form']],
                    domain: ['&', ['create_date', '>=', past], ['type', '=', 'product']],
                    target: 'current'
                });
            },

            getCurrentDate: function () {
                const today = new Date();

                return today;
            }
        });

        /* Registering the client action. */
        core.action_registry.add('product_dashboard', ProductDashboard);

        return ProductDashboard;
    }
);
